<?php

use App\Models\Room;
use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $fake = Faker\Factory::create();
        for($i =0; $i<50; $i++){
            Room::create([
                'name'=>$fake->name,
                'cover'=>$fake->imageUrl(160,220),
                'synopsis'=>$fake->paragraph
            ]);
        }
    }
}
