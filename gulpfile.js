const elixir = require('laravel-elixir');

require('laravel-elixir-webpack-react');

elixir(mix => {
    //Output: public/*
    /*=======================================
     | SASS | source: resources/assets/sass
     ========================================*/
    mix.sass('app.scss');

    /*=======================================
     | JS | source: resources/assets/js
     ========================================*/
    mix.webpack('app.js');
});
