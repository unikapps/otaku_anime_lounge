<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Otalu Anime Lounge</title>
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="/css/app.css.map">
    </head>
    <body>
      <header></header>
      <div id="container">
        <div class="loading"></div>
      </div>
      <footer></footer>

      <script src="/js/libs/jquery-2.2.0.min.js"></script>
      <script src="/js/libs/slick.min.js"></script>
      <script src="/js/libs/moment.min.js"></script>
      <script src="/js/app.js"></script>
    </body>
</html>
