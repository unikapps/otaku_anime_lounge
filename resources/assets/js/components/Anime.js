import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Cover from './Cover';
import Info from './Info';

class Anime extends Component {
    render() {
        return (
            <div className="anime">
                <Cover cover={this.props.anime.cover} />
                <Info name={this.props.anime.name} createdAt={this.props.anime.createdAt} synopsis={this.props.anime.synopsis} />
            </div>
        );
    }
}

export default Anime;
