import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Cover extends Component {
    render() {
        return (
            <figure>
                <img src={this.props.cover} />
            </figure>
        );
    }
}

export default Cover;
