import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Info extends Component {
    render() {
        return (
            <div className="infos">
                <h4>{this.props.name}</h4>
                <h4>{this.props.createdAt}</h4>
                <p>{this.props.synopsis}</p>
            </div>
        );
    }
}

export default Info;
