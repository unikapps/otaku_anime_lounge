import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Anime from './Anime';

class Rooms extends Component {
  componentDidMount() {
    $(".loading").hide();
    $(".rooms-list").slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
    });
  }

  render() {
    var rows = [];
    this.props.rooms.forEach((room) => {
      room.createdAt = moment(room.created_at).format("MMM Do YYYY");
      rows.push(<Anime anime={room} key={room.id} />);
    });
    return (
      <div className="rooms">
        <div className="rooms-list slider">{rows}</div>
      </div>
    );
  }
}

export default Rooms;

// render rooms Component with data from api
$.get("/api/rooms", function(data, status){
  var roomsFromAPI = data;

  ReactDOM.render(
    <Rooms rooms={roomsFromAPI} />,
    document.getElementById('container')
  );
});
