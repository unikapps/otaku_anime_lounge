# Otaku Amine Lounge

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

# Environment #

## Apps:##
* XAMPP for PHP & MYSQL
* Composer 
* Nodejs (v6.9.1) with npm (3.10.10)

### Setup ###
I prefer to use virtual host, so first thing i append my vhost config to C:\xampp\apache\conf\extra\httpd-vhosts.conf using [this snippet](https://gist.github.com/rachid804/93faf9f60816304ad025), then add it to hosts.

In command line (i prefer [Cmnder](http://cmder.net/) in windows ) navigate to the project and run 

\>**composer install**

change DB configuration in .env
To create database structure run

\>**php artisan migrate**

To add some data to work with run

\>**php artisan db:seed**

For the front end i added npm dependencies to use React with [Laravel elixir](https://laravel.com/docs/5.3/elixir)  [following this article](https://blog.tighten.co/adding-react-components-to-a-laravel-app), if you want to use it it's very easy all you need to do is to install dependencies 

\>**npm install**

Then 

\>**npm run dev**

Please check package.js and gulpfile.js for more details. 
 if you want to use your own config that's totally fine, just store compiled assets in public/.
 
## Contribution
To start a created a very basic API to list chat rooms: end point http://project.url/api/rooms and the view is in resources/views/layouts/master.blade.php

For info laravel use {{}} as php echo so to use is with React you need to prefix it with @ like @{{}}